import { Character } from "../core/character";
import { Enchant } from "./enchant";

export class Mighty extends Enchant {


    attack(target:Character):string {        
        target.hp -= 5;
        return super.attack(target);
    }   
}