import { Character } from "../core/character";
import { IWeapon } from "../strategy/iweapon";


export abstract class Enchant implements IWeapon {
    protected holder:Character;
    constructor(protected enchanted:IWeapon){}

    attack(target: Character): string {
        return this.enchanted.attack(target);
    }
    setHolder(holder: Character): void {
        this.holder = holder;
        this.enchanted.setHolder(holder);
    }

}