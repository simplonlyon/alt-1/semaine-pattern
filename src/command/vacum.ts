

export class Vacum {

    private startHour:number;
    private vacuming = false;
    
    program(hour: number) {
        this.startHour = hour;
    }

    start() {
        this.vacuming = true;
    }

}