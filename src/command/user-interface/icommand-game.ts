

export interface ICommandGame {
    execute():ICommandGame[];
    getName():string;
}